class Bitfield {
	constructor (fieldNames, init = 0n) {
		this.store = init;
		this.fieldNames = fieldNames;
		this.fieldLocations = {};
		for (let i = 0n; i < fieldNames.length; i++) {
			this.fieldLocations[fieldNames[i]] = i;
		}
	}
	get (field) {
		return !!(this.store & (1n << this.fieldLocations[field]));
	}
	set (field, setTrue) {
		let bit = 1n << this.fieldLocations[field];
		if (setTrue) {
			this.store |= bit;
		} else {
			this.store &= ~bit;
		}
	}
	forEach (callback = (field, isTrue) => {}) {
		let fields = Object.keys(this.fieldLocations);
		for (let i = 0; i < fields.length; i++) {
			callback(fields[i], this.get(fields[i]));
		}
	}
}

export { Bitfield };